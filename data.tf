# # SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

data "google_client_config" "provider" {}

data "google_container_cluster" "dyff" {
  name     = local.cluster_name
  location = local.region
}

data "terraform_remote_state" "storage" {
  backend = "http"
  config = {
    address  = "https://gitlab.com/api/v4/projects/55591694/terraform/state/${var.environment}"
    username = var.remote_state_user
    password = var.remote_state_password
  }
}
