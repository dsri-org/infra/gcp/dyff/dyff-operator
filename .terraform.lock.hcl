# This file is maintained automatically by "tofu init".
# Manual edits may be lost in future updates.

provider "registry.opentofu.org/hashicorp/google" {
  version     = "5.21.0"
  constraints = "~> 5.21.0"
  hashes = [
    "h1:yfJCWOSg8Is2nmoKB2O+cKbsXcNzBw/8UbRFoRJXRls=",
    "zh:1d43bf3e247fe4265ffbc1c5452c66adc2566fc75ee4f500602b182ffedcbae4",
    "zh:5b0800b7e7c69fbd5128adc34ef537b168e46c4aa8f977679bc5628418a8f93d",
    "zh:5fc9289835d00350dd328009661f9b1b9a7de3d15d1f559f51957ce54fcefa6c",
    "zh:8ff8981cae62342988d8cc1e0889de7987956b8ae57466ba6a68037711584a04",
    "zh:a3b5d625bedab2e91446f09a5f7491e44e389afbefa2a80af4841871687b80b7",
    "zh:aa88e0afe7a28438d97a51bce0a10460d24064a2d9d9f1185e00b3774a523fb0",
    "zh:af881c3537687658b5be02bc2e2f272865a3d54a7f4170b46d537be655bdea62",
    "zh:e8b976fa6fa070582eef61f23cd7f2a84247c8fe39861cb5ae59a7217d369851",
    "zh:f7da2d1c220bd6177704cb76176859e6e655633e793ddbf004589cb832e90079",
    "zh:fb3f9dc66a0a281b08935b29d83c8c7a4636aa8ae1bdba01901dd5a57ba6c3d6",
  ]
}

provider "registry.opentofu.org/hashicorp/helm" {
  version     = "2.12.1"
  constraints = "~> 2.12.1"
  hashes = [
    "h1:S0+5VN/viVA4YYpm9q45bZ903EqP3bwjv5abps+a3lE=",
    "zh:0349149992646530c33314cb973eba68757606a037017ba47e56db695d4b3afe",
    "zh:3138ffe23c481b01419a4a21adf83538efe6e698b421c4a8f7d142b198518709",
    "zh:44658e3070405b88fbd76161ecddde62f478dc31aaebee3b93c2f2783a6d45f9",
    "zh:5600a3407dfb8b77da7561490157afa8ad505c864a5dd35ed8d678e9ad8378ca",
    "zh:6445e359c813ecbb7c2edf722ed0d1f33dfb171b6a7b470f40cf1e24045b7441",
    "zh:7973054604c7f5a51600f6e63fa0327d05b29fac2bffd222c21660cbdd2939f9",
    "zh:7c59e2d4602ab5d9de0ba8e442ec1fc425c8f143581018d1e7f645298a124f01",
    "zh:8c0fb411dd5de664ac5e801d70507781790c4fc196518a56966d66d0963c240c",
    "zh:a6a988c91bbf1828a8fc55001f10c7d06c5c53dc718ee7cd6814bdfa2e6652e0",
    "zh:b7935d7dacd7e5a91ff9d17cfb04ce88c9100e563fd88487d14519e8d8d8b2e1",
  ]
}

provider "registry.opentofu.org/hashicorp/kubernetes" {
  version     = "2.27.0"
  constraints = "~> 2.27.0"
  hashes = [
    "h1:Jtbdvbq8kIXUENtH3tVwgcjHqbuYp1pGfg4gFocY+e4=",
    "zh:1146f53fb39fd4bcea5574303c4871001a97d7891f65a60a4ecbc64da2a90d75",
    "zh:1f7e3dc0dbb854f56a0f5ba3c50588272984ae9775da027c3c7f32cb6d8245b0",
    "zh:2166f7fdade75266658603280bc822edab848e52a674340485847dde1c5d9324",
    "zh:21a97530857330d2013aa66fb7afebb44fe4a5543418d0a3ca93750acd11fea5",
    "zh:2d4b9fea7e99750647e1cd8df9a67cba45905825867dd19ab01411dad6b8c6fd",
    "zh:de30e92e638b95e56dbb2232cb9a6f6a69346ecb3644965e9be715eaf29f22ff",
    "zh:f4ae951c9add4349a498f44c3f5768cbaf7a966392a0e7632de288889e7cd5d9",
    "zh:f54ecb1917dfa198933d72632ea6f0aa4da3ead070d6b9765ec1d3b7da60e827",
    "zh:fba8a2f192eb5fe248708b9037db046e0d9176e7c54c6edc6f6aa55d50474082",
    "zh:fe525956f3e54f0bbd2891a6abad1f807b4763b8dc734d810e223876741fefa3",
  ]
}
