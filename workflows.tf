# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

resource "kubernetes_namespace" "workflows" {
  metadata {
    # It should be possible to configure the application to use arbitrary namespaces.
    name = "workflows"
    labels = {
      # https://kubernetes.io/docs/concepts/security/pod-security-standards/
      "pod-security.kubernetes.io/enforce" = "restricted"
    }
  }
}

resource "kubernetes_config_map" "workflows" {
  metadata {
    # It should be possible to configure the application to use arbitrary config maps.
    # TODO: support changing this name
    name      = "workflows-config"
    namespace = kubernetes_namespace.workflows.metadata[0].name
  }

  data = merge(local.storage_urls, {
    DYFF_STORAGE__BACKEND      = "dyff.storage.backend.s3.storage.S3StorageBackend"
    DYFF_STORAGE__S3__ENDPOINT = local.storage.hostname
    AWS_ENDPOINT_URL           = local.storage.hostname
  })
}

resource "kubernetes_secret" "workflows" {
  metadata {
    # It should be possible to configure the application to use arbitrary secrets.
    # TODO: support changing this name
    name      = "workflows-storage-credentials"
    namespace = kubernetes_namespace.workflows.metadata[0].name
  }

  data = {
    DYFF_STORAGE__S3__ACCESS_KEY = local.storage.access_id
    DYFF_STORAGE__S3__SECRET_KEY = local.storage.secret
    AWS_ACCESS_KEY_ID            = local.storage.access_id
    AWS_SECRET_ACCESS_KEY        = local.storage.secret
  }
}

resource "kubernetes_secret" "workflows_models_fetch_model" {
  metadata {
    # It should be possible to configure the application to use arbitrary secrets.
    # TODO: support changing this name
    name      = "model-fetcher-external-credentials"
    namespace = kubernetes_namespace.workflows.metadata[0].name
  }

  data = {
    DYFF_MODELS__HUGGINGFACE_ACCESS_TOKEN = local.models.huggingface_access_token
  }
}

resource "kubernetes_network_policy" "untrusted_pods" {
  metadata {
    name      = "untrusted-pods"
    namespace = kubernetes_namespace.workflows.metadata[0].name
  }

  spec {
    egress {
      ports {
        port     = 988
        protocol = "TCP"
      }

      to {
        ip_block {
          cidr = "169.254.169.252/32"
        }
      }
    }

    egress {
      ports {
        port     = 80
        protocol = "TCP"
      }

      to {
        ip_block {
          cidr = "169.254.169.254/32"
        }
      }
    }

    pod_selector {
      match_expressions {
        key      = "security.dyff.io/untrusted"
        operator = "Exists"
      }
    }

    policy_types = [
      "Egress",
      "Ingress",
    ]
  }
}
