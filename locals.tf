# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

locals {
  cluster_name = "${var.environment}-dyff-cloud"
  deployment   = "dyff-operator"
  name         = "${var.environment}-${local.deployment}"
  region       = "us-central1"

  default_tags = {
    deployment  = local.deployment
    environment = var.environment
  }

  crd_path  = "${path.module}/crds/"
  crd_files = fileset("${local.crd_path}", "*.yaml")
  versions  = yamldecode(file("${path.module}/version.yaml"))

  models = {
    huggingface_access_token = var.huggingface_access_token
  }

  storage = data.terraform_remote_state.storage.outputs

  storage_urls = {
    DYFF_RESOURCES__DATASETS__STORAGE__URL     = local.storage.buckets["datasets"].s3_url
    DYFF_RESOURCES__MEASUREMENTS__STORAGE__URL = local.storage.buckets["measurements"].s3_url
    DYFF_RESOURCES__MODELS__STORAGE__URL       = local.storage.buckets["models"].s3_url
    DYFF_RESOURCES__MODULES__STORAGE__URL      = local.storage.buckets["modules"].s3_url
    DYFF_RESOURCES__OUTPUTS__STORAGE__URL      = local.storage.buckets["outputs"].s3_url
    DYFF_RESOURCES__REPORTS__STORAGE__URL      = local.storage.buckets["reports"].s3_url
    DYFF_RESOURCES__SAFETYCASES__STORAGE__URL  = local.storage.buckets["safetycases"].s3_url
  }
}
