# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

resource "kubernetes_namespace" "dyff_operator" {
  metadata {
    name = "dyff-operator"
    labels = {
      # https://kubernetes.io/docs/concepts/security/pod-security-standards/
      "pod-security.kubernetes.io/enforce" = "restricted"
    }
  }
}

# https://artifacthub.io/packages/helm/dyff-operator/dyff-operator
resource "helm_release" "dyff_operator" {
  name       = "dyff-operator"
  namespace  = kubernetes_namespace.dyff_operator.metadata[0].name
  repository = "oci://registry.gitlab.com/dyff/charts"
  chart      = "dyff-operator"

  # get dyff-operator version from versions.yaml to synchronize with CRDs
  version = local.versions.dyff_operator

  skip_crds = true

  wait          = false
  wait_for_jobs = false

  values = [yamlencode({
    crds = {
      enabled = false
    }

    extraEnvVarsConfigMap = {}

    reports = {
      # SECURITY: Production configurations MUST set this value:
      runtimeClassName = "gvisor"
    }

    resources = {
      limits = {
        cpu                 = "750m"
        memory              = "768Mi"
        "ephemeral-storage" = "1024Mi"
      }
      requests = {
        cpu                 = "500m"
        memory              = "512Mi"
        "ephemeral-storage" = "50Mi"
      }
    }

    storage = {
      backend = "s3"

      s3 = {
        accessKey = local.storage.access_id

        external = {
          endpoint = local.storage.hostname
        }

        internal = {
          enabled = false
        }
      }

      urls = {
        datasets     = local.storage.buckets["datasets"].s3_url
        measurements = local.storage.buckets["measurements"].s3_url
        models       = local.storage.buckets["models"].s3_url
        modules      = local.storage.buckets["modules"].s3_url
        outputs      = local.storage.buckets["outputs"].s3_url
        reports      = local.storage.buckets["reports"].s3_url
        safetycases  = local.storage.buckets["safetycases"].s3_url
      }
    }
  })]

  depends_on = [kubernetes_manifest.crds]
}

resource "kubernetes_manifest" "crds" {
  for_each = local.crd_files
  manifest = yamldecode(file("${local.crd_path}${each.key}"))
}

resource "kubernetes_service_account" "evaluation_client" {
  metadata {
    namespace = "workflows"
    name      = "evaluation-client"
  }
}

resource "kubernetes_service_account" "model_fetcher" {
  metadata {
    # FIXME: dyff-storage binds this KSA using the hard-coded namespace/name
    # Don't change the namespace/name.
    namespace = "workflows"
    name      = "model-fetcher"
  }
}

resource "kubernetes_service_account" "report_runner" {
  metadata {
    namespace = "workflows"
    name      = "report-runner"
  }
}

resource "kubernetes_storage_class" "dyff_model" {
  metadata {
    name = "dyff-model"
  }

  storage_provisioner = "pd.csi.storage.gke.io"
  volume_binding_mode = "Immediate"
  # Expansion seems to cause problems with read-only volumes
  allow_volume_expansion = false
  parameters = {
    type = "pd-standard"
  }
}

resource "kubernetes_service_account" "inferencesession_runner" {
  metadata {
    # FIXME: dyff-storage binds this KSA using the hard-coded namespace/name
    # Don't change the namespace/name.
    namespace = "workflows"
    name      = "inferencesession-runner"
  }
}
