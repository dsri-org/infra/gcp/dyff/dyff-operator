#!/bin/bash
# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

set -euo pipefail

kubectl get crd | grep dyff.io | awk '{print $1}' | xargs kubectl delete crd
