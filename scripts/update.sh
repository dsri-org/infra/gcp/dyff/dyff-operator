#!/bin/sh
# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

set -eu

BASE_DIR="$(realpath "$(dirname "$0")/..")"
VERSION="$(cat ${BASE_DIR}/version.yaml | awk '/dyff_operator/ {print $2}')"

echo "$BASE_DIR"

# fail if k8split not available
# https://github.com/brendanjryan/k8split
which k8split
rm -f crds/*.yaml
mkdir -p crds/

helm template oci://registry.gitlab.com/dyff/charts/dyff-operator \
    --version "$VERSION" \
    --show-only 'templates/crds/*' | sed -e '/creationTimestamp/d' > manifest.yaml

k8split -o crds/ manifest.yaml

rm -f manifest.yaml

for f in ./crds/customresourcedefinition-* ; do
    g="$(echo "$f" | sed -e 's/customresourcedefinition-//g')"
    mv "$f" "$g"
done

npx prettier -w crds/
