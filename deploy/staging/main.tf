# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

module "root" {
  source = "../.."

  google_cloud_service_account_file = var.google_cloud_service_account_file
  environment                       = "staging"
  project                           = "staging-dyff-659606"
  remote_state_user                 = var.remote_state_user
  remote_state_password             = var.remote_state_password
  huggingface_access_token          = var.huggingface_access_token
}
